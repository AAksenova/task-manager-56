package ru.t1.aksenova.tm.service;

import liquibase.Liquibase;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.aksenova.tm.api.service.IAdminService;
import ru.t1.aksenova.tm.api.service.ILiquibaseService;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.exception.user.AccessDeniedException;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class AdminService implements IAdminService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ILiquibaseService LiquibaseService;

    @NotNull
    @Override
    @SneakyThrows
    public String getDropScheme(@Nullable final String token) {
        @NotNull final String initToken = propertyService.getDatabaseInitToken();
        if (!initToken.equals(token) || token == null || initToken == null) throw new AccessDeniedException();
        @NotNull final String liquibaseProperty = propertyService.getLiquibasePropertiesFilename();
        @NotNull final Liquibase liquibase = LiquibaseService.liquibase(liquibaseProperty);
        try {
            liquibase.dropAll();
            return "SUCCESS";
        } catch (@NotNull final Exception e) {
            return "ERROR";
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public String getInitScheme(@Nullable final String token) {
        @NotNull final String initToken = propertyService.getDatabaseInitToken();
        if (!initToken.equals(token) || token == null || initToken == null) throw new AccessDeniedException();
        @NotNull final String liquibaseProperty = propertyService.getLiquibasePropertiesFilename();
        @NotNull final Liquibase liquibase = LiquibaseService.liquibase(liquibaseProperty);
        try {
            liquibase.update("scheme");
            return "SUCCESS";
        } catch (@NotNull final Exception e) {
            return "ERROR";
        }
    }

}
