package ru.t1.aksenova.tm.api.service.model;

import ru.t1.aksenova.tm.api.repository.model.IRepository;
import ru.t1.aksenova.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {
}
