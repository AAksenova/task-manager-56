package ru.t1.aksenova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.aksenova.tm.dto.model.TaskDTO;
import ru.t1.aksenova.tm.dto.request.TaskListByProjectIdRequest;
import ru.t1.aksenova.tm.dto.response.TaskListByProjectIdResponse;
import ru.t1.aksenova.tm.util.TerminalUtil;

import java.util.Collections;
import java.util.List;

@Component
public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-project-id";

    @NotNull
    public static final String DESCRIPTION = "Display tasks from project by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();

        @Nullable final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(getToken());
        request.setProjectId(projectId);
        @Nullable final TaskListByProjectIdResponse response = getTaskEndpoint().listTaskByProjectId(request);
        if (response.getTasks() == null) response.setTasks(Collections.emptyList());
        @Nullable final List<TaskDTO> tasks = response.getTasks();
        renderTasks(tasks);
    }

}
